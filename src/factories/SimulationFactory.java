package factories;

import controllers.SettingsController;
import controllers.SimulationController;
import factories.game.PlayersFactory;
import factories.game.cards.DeckOfCardsGenerator;
import models.Settings;
import models.Simulation;

public class SimulationFactory {
    public static Simulation generateSimulation(Settings settings, SimulationController controller) {
        return new Simulation(
                settings,
                PlayersFactory.generatePlayers(settings.getPlayerNames()),
                (new TrajectoryGenerator(settings.getMatrixDimension())).getTrajectory(),
                DeckOfCardsGenerator.getDeckOfCards(),
                controller
        );
    }
}
