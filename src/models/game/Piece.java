package models.game;

import controllers.MovementHistoryController;
import interfaces.abilities.Positionable;
import javafx.application.Platform;
import models.MoveDescription;
import models.Position;
import models.Simulation;
import models.game.assets.Diamond;
import models.game.assets.Hole;
import models.game.cards.MoveCard;
import ui_components.Cell;
import util.PieceColor;

import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Piece implements Positionable {
    protected PieceColor color;
    protected PieceHistory pieceHistory;

    protected Position currentPosition;
    protected Position nextPosition;

    protected Player player;

    protected int pieceNumber;

    protected int numberOfDiamonds;

    private boolean isFinished;

    public Piece() {

    }

    public Piece(PieceColor color) {
        this.color = color;
        this.numberOfDiamonds = 0;
        this.pieceHistory = new PieceHistory(this);
    }

    public boolean isFinished() {
        return isFinished;
    }


    public abstract Cell getNewCell();

    private int getSteps(int steps) {
        return steps + this.numberOfDiamonds;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setPieceNumber(int pieceNumber) {
        this.pieceNumber = pieceNumber;
    }

    public Player getPlayer() {
        return player;
    }

    public int getNumberOfDiamonds() {
        return numberOfDiamonds;
    }

    public int getPieceNumber() {
        return pieceNumber;
    }

    public PieceHistory getPieceHistory() {
        return pieceHistory;
    }

    public void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public void await(CountDownLatch latch) {
        try {
            latch.await();
        } catch (InterruptedException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public void move(int steps, MoveDescription moveDescription) {
        Position startPosition = this.currentPosition;

        moveDescription
                .setStartPosition(startPosition)
                .setHasFellIntoHoleOnStartPosition(false)
                .setFellIntoHole(false)
                .setNumberOfFields(this.getSteps(steps));

        if (this.currentPosition != null) {
//            this.currentPosition.setLocked(true);
            CountDownLatch latch = new CountDownLatch(1);

            this.currentPosition.setCell(Simulation.SIMULATION.getTrajectory().getNewCell());
            this.currentPosition.setElement(null);
            this.currentPosition.setLatchToWaitForJavaFx(latch);
            Platform.runLater(this.currentPosition);
            this.await(latch);
//            this.currentPosition.setLocked(false);

            if (this.currentPosition.getAsset() != null) {
                if (this.currentPosition.getAsset() instanceof Hole) {
                    this.handleHole(this.currentPosition, moveDescription);
                    return;
                }
            }
        }

        this.nextPosition = Simulation.SIMULATION.getTrajectory().getNextPosition(this.currentPosition, this.getSteps(steps));
        while (this.nextPosition != null && this.nextPosition.getElement() != null) {
            this.nextPosition = Simulation.SIMULATION.getTrajectory().getNextPosition(this.nextPosition, 1);
        }

        if (this.nextPosition == null) {
            moveDescription.setFinished(true);
            this.pieceHistory.setFinished();
            this.isFinished = true;
            return;
        }

//        this.nextPosition.setLocked(true);

        CountDownLatch latch = new CountDownLatch(1);

        this.nextPosition.setCell(this.getNewCell());
        this.nextPosition.setElement(this);
        this.nextPosition.setLatchToWaitForJavaFx(latch);
        Platform.runLater(this.nextPosition);
        this.await(latch);

        this.currentPosition = this.nextPosition;

        moveDescription.setNextPosition(this.nextPosition);
        this.pieceHistory.stepOnPosition(this.nextPosition);

        if (this.nextPosition.getAsset() != null) {
            if (this.nextPosition.getAsset() instanceof Hole) {
               this.handleHole(nextPosition, moveDescription);
            }

            if (this.nextPosition.getAsset() instanceof Diamond) {
                CountDownLatch latch2 = new CountDownLatch(1);

                this.nextPosition.setCell(Simulation.SIMULATION.getTrajectory().getNewCell());
                this.nextPosition.setLatchToWaitForJavaFx(latch2);
                this.nextPosition.setAsset(null);
                Platform.runLater(this.currentPosition);
                this.await(latch2);
                this.numberOfDiamonds++;
            }
        }
    }

    protected void handleHole(Position position, MoveDescription moveDescription) {
        this.isFinished = true;
        moveDescription.setHasFellIntoHoleOnStartPosition(true);
        CountDownLatch latch2 = new CountDownLatch(1);

        position.setCell(Simulation.SIMULATION.getTrajectory().getNewCell());
        position.setElement(null);
        position.setLatchToWaitForJavaFx(latch2);
        position.setAsset(null);
        Platform.runLater(position);
        this.await(latch2);
    }

    public void updateDescription(Position currentPosition) {

    }

}
