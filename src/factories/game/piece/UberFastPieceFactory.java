package factories.game.piece;

import interfaces.functional.PieceGeneratable;
import models.game.Piece;
import models.game.pieces.UberFastPiece;
import util.PieceColor;

public class UberFastPieceFactory implements PieceGeneratable {
    @Override
    public Piece generate(PieceColor color) {
        return new UberFastPiece(color);
    }
}
