package models.game.pieces;

import javafx.scene.image.Image;
import models.MoveDescription;
import models.game.Piece;
import ui_components.Cell;
import ui_components.ImageCell;
import util.PieceColor;

public class UberFastPiece extends Piece {
    public UberFastPiece(PieceColor color) {
        super(color);
    }

    @Override
    public Cell getNewCell() {
        return new ImageCell(
                new Image(
                        String.format("resources/images/pieces/%s/uber_fast_piece.png", this.color.getColor())
                )
        );
    }

    @Override
    public void move(int steps, MoveDescription moveDescription) {
        super.move(steps*2, moveDescription);
    }


}
