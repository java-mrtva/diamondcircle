package ui_components;

import javafx.scene.control.ListCell;
import models.game.Piece;

public class PieceListCell extends ListCell<Piece> {
    @Override
    public void updateItem(Piece piece, boolean empty) {
        super.updateItem(piece, empty);
        if (empty) {
            setGraphic(null);
            setText(null);
        } else {
            Cell image = piece.getNewCell();
            image.setMaxHeight(20);
            image.setMaxWidth(20);
            setGraphic(image);
            setText(piece.getPlayer().getName() + ": " + "Piece " + piece.getPieceNumber());
        }
    }

    @Override
    protected double computeMaxHeight(double v) {
        return 20;
    }
}