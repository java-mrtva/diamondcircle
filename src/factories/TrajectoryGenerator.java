package factories;

import models.Position;
import models.Trajectory;

import java.util.concurrent.CopyOnWriteArrayList;

public class TrajectoryGenerator {
    private final int matrixDimension;

    enum Section {
        START,
        NORTH_EAST,
        SOUTH_EAST,
        SOUTH_WEST,
        NORTH_WEST,
    }

    private int cycle;

    private Section currentSection;

    public TrajectoryGenerator(int matrixDimension) {
        this.matrixDimension = matrixDimension;
        this.cycle = 0;
        this.currentSection = Section.NORTH_WEST;
    }

    private int getMinBorder() {
        return this.cycle;
    }

    private int getMaxBorder() {
        return this.matrixDimension - this.cycle - 1;
    }

    private Position getNext(Position position) {
        return switch (this.currentSection) {
            case START -> new Position(position.getX(), position.getY() + 1);
            case NORTH_EAST -> new Position(position.getX() + 1, position.getY() + 1);
            case SOUTH_EAST -> new Position(position.getX() + 1, position.getY() - 1);
            case SOUTH_WEST -> new Position(position.getX() - 1, position.getY() - 1);
            case NORTH_WEST -> new Position(position.getX() - 1, position.getY() + 1);
        };
    }

    public Trajectory getTrajectory() {

        CopyOnWriteArrayList<Position> trajectory = new CopyOnWriteArrayList<>();
        Position position = new Position(0, (this.matrixDimension - 1) / 2 - 1);

        while (true) {
            Position nextPosition = this.getNext(position);
            if (trajectory.contains(nextPosition) && this.currentSection == Section.NORTH_WEST) {
                this.currentSection = Section.START;
                position = this.getNext(position);
                if (trajectory.contains(position)) {
                    break;
                } else {
                    trajectory.add(position);
                    this.currentSection = Section.NORTH_EAST;
                }
                this.cycle++;
                continue;
            }
            if (position.getX() == this.getMinBorder() && this.currentSection == Section.NORTH_WEST) {
                this.currentSection = Section.START;
            } else if (position.getX() == this.getMinBorder() && this.currentSection == Section.START) {
                this.currentSection = Section.NORTH_EAST;
            } else if (position.getY() == this.getMaxBorder()) {
                this.currentSection = Section.SOUTH_EAST;
            } else if (position.getX() == this.getMaxBorder()) {
                this.currentSection = Section.SOUTH_WEST;
                if (trajectory.contains(this.getNext(position))) {
                    this.currentSection = Section.NORTH_WEST;
                }
            } else if (position.getY() == this.getMinBorder()) {
                this.currentSection = Section.NORTH_WEST;
                if (trajectory.contains(this.getNext(position))) {
                    this.currentSection = Section.START;
                    this.cycle++;
                }
            }

            position = this.getNext(position);
            if (trajectory.contains(position)) {
                break;
            }
            trajectory.add(position);

        }

        return new Trajectory(trajectory);
    }
}
