package util;

import config.Config;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Validator {
    private Map<TextField, Label> fieldErrorLabelMap;
    private Map<TextField, ArrayList<String>> fieldErrorsMap;


    public Validator(Map<TextField, Label> fieldErrorLabelMap) {
        this.fieldErrorLabelMap = fieldErrorLabelMap;
        this.fieldErrorsMap = new HashMap<>();
        this.fieldErrorLabelMap.forEach((field, label) ->
            fieldErrorsMap.put(field, new ArrayList<>())
        );
    }

    public boolean validate() {
        this.fieldErrorLabelMap.forEach((field, label) ->
                fieldErrorsMap.put(field, new ArrayList<>())
        );
        this.fieldErrorsMap.forEach((field, errorList) -> {
            if(!validateRequired(field)) {
                errorList.add(Literals.RULE_REQUIRED);
            } else {
                errorList.remove(Literals.RULE_REQUIRED);
            }
            if(!validateUnique(field, this.fieldErrorLabelMap.keySet())) {
                errorList.add(Literals.RULE_UNIQUE);
            } else {
                errorList.remove(Literals.RULE_UNIQUE);
            }
            if(!validateMaxLength(field)) {
                errorList.add(Literals.RULE_MAX_LENGTH);
            } else {
                errorList.remove(Literals.RULE_MAX_LENGTH);
            }
        });
        this.setErrorLabels();
        return this.isFormValid();
    }

    public boolean isFormValid() {
        return this.fieldErrorsMap.keySet().stream().allMatch(field ->
                this.fieldErrorsMap.get(field).isEmpty()
        );
    }

    private boolean isFieldValid(TextField field) {
        return this.fieldErrorsMap.get(field).isEmpty();
    }

    private void setErrorLabels() {
        this.fieldErrorLabelMap.forEach((field, errorLabel) -> {
            if(!this.isFieldValid(field)) {
                errorLabel.setText(Literals.ERROR_LABELS.get(this.fieldErrorsMap.get(field).get(0)));
                errorLabel.setVisible(true);
            } else {
                errorLabel.setVisible(false);
            }
        });
    }
    private boolean validateRequired(TextField field) {
        return !field.getText().isEmpty();
    }

    private boolean validateUnique(TextField field, Set<TextField> otherFields) {
        return otherFields.stream().filter(x -> !x.equals(field)).noneMatch(f -> f.getText().equals(field.getText()));
    }

    private boolean validateMaxLength(TextField field)
    {
        return field.getText().length() <= Config.MAX_STRING_INPUT_LENGTH;
    }
}
