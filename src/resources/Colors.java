package resources;

public class Colors {
    public static final String EMPTY_CELL = "white";
    public static final String TRAJECTORY_COLOR = "#abc4ce";

    public static final String PIECE_COLOR_RED = "red";
    public static final String PIECE_COLOR_GREEN = "green";
    public static final String PIECE_COLOR_BLUE = "blue";
    public static final String PIECE_COLOR_YELLOW = "yellow";
}
