package factories.game.piece;

import interfaces.functional.PieceGeneratable;
import models.game.Piece;
import models.game.pieces.OrdinaryPiece;
import util.PieceColor;

public class OrdinaryPieceFactory implements PieceGeneratable {
    @Override
    public Piece generate(PieceColor color) {
        return new OrdinaryPiece(color);
    }
}
