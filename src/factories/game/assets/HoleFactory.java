package factories.game.assets;

import interfaces.abilities.Collectable;
import interfaces.functional.AssetGeneratable;
import models.game.assets.Hole;

public class HoleFactory implements AssetGeneratable {
    @Override
    public Collectable generate() {
        return new Hole();
    }
}
