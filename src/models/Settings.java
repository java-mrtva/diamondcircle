package models;

import java.util.List;

public class Settings {
    public Settings(int numberOfPlayers, int matrixDimension, List<String> playerNames) {
        this.numberOfPlayers = numberOfPlayers;
        this.matrixDimension = matrixDimension;
        this.playerNames = playerNames;
    }

    private final int numberOfPlayers;
    private final int matrixDimension;

    private final List<String> playerNames;

    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    public int getMatrixDimension() {
        return matrixDimension;
    }

    public List<String> getPlayerNames() {
        return playerNames;
    }
}
