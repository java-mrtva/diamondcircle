package controllers;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import config.Config;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import models.Settings;
import models.Simulation;
import util.Validator;

public class SettingsController {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnExit;

    @FXML
    private Button btnStart;

    @FXML
    private Label lblPlayer1;

    @FXML
    private Label lblPlayer2;

    @FXML
    private Label lblPlayer3;

    @FXML
    private Label lblPlayer4;

    @FXML
    private Label lblPlayer1Error;

    @FXML
    private Label lblPlayer2Error;

    @FXML
    private Label lblPlayer3Error;

    @FXML
    private Label lblPlayer4Error;

    @FXML
    private Spinner<Integer> sprDimension;

    @FXML
    private Spinner<Integer> sprNumberOfPlayers;

    @FXML
    private TextField tfPlayer1Name;

    @FXML
    private TextField tfPlayer2Name;

    @FXML
    private TextField tfPlayer3Name;

    @FXML
    private TextField tfPlayer4Name;

    private Validator validator;
    private Map<TextField, Label> playerFieldErrorLabelMap;
    private Map<TextField, Label> playerFieldLabelMap;
    private Map<TextField, String > playerFieldDefaultValueMap;
    private List<TextField> playerFields;

    private boolean isFormValid;


    @FXML
    void exitSimulation(ActionEvent event) {
        System.exit(1);
    }

    @FXML
    void startSimulation(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/views/simulation.fxml"));

            Settings settings = new Settings(
                    this.sprNumberOfPlayers.getValue(),
                    this.sprDimension.getValue(),
                    this.playerFields.stream().map(TextInputControl::getText).limit(this.sprNumberOfPlayers.getValue()).toList());

            loader.setController(new SimulationController(settings));

            Scene scene = new Scene(loader.load());
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Diamond Circle");
            stage.show();

            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    Simulation.RUNNING = false;
                    Simulation.PAUSED = true;
                }
            });


        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    @FXML
    void onPlayerNameChanged(KeyEvent keyEvent) {
        this.validate();
    }

    void validate()
    {
        this.isFormValid = this.validator.validate();
        this.btnStart.setDisable(!isFormValid);
    }

    @FXML
    void initialize() {
        assert btnExit != null : "fx:id=\"btnExit\" was not injected: check your FXML file 'Untitled'.";
        assert btnStart != null : "fx:id=\"btnStart\" was not injected: check your FXML file 'Untitled'.";
        assert lblPlayer1Error != null : "fx:id=\"lblPlayer1Error\" was not injected: check your FXML file 'Untitled'.";
        assert lblPlayer2Error != null : "fx:id=\"lblPlayer2Error\" was not injected: check your FXML file 'Untitled'.";
        assert lblPlayer3Error != null : "fx:id=\"lblPlayer3Error\" was not injected: check your FXML file 'Untitled'.";
        assert lblPlayer4Error != null : "fx:id=\"lblPlayer4Error\" was not injected: check your FXML file 'Untitled'.";
        assert sprDimension != null : "fx:id=\"sprDimension\" was not injected: check your FXML file 'Untitled'.";
        assert sprNumberOfPlayers != null : "fx:id=\"sprNumberOfPlayers\" was not injected: check your FXML file 'Untitled'.";
        assert tfPlayer1Name != null : "fx:id=\"tfPlayer1Name\" was not injected: check your FXML file 'Untitled'.";
        assert tfPlayer2Name != null : "fx:id=\"tfPlayer2Name\" was not injected: check your FXML file 'Untitled'.";
        assert tfPlayer3Name != null : "fx:id=\"tfPlayer3Name\" was not injected: check your FXML file 'Untitled'.";
        assert tfPlayer4Name != null : "fx:id=\"tfPlayer4Name\" was not injected: check your FXML file 'Untitled'.";

        sprDimension.setValueFactory(
                new SpinnerValueFactory.IntegerSpinnerValueFactory(
                        Config.MIN_DIMENSION,
                        Config.MAX_DIMENSION,
                        Config.MIN_DIMENSION
                )
        );
        sprNumberOfPlayers.setValueFactory(
                new SpinnerValueFactory.IntegerSpinnerValueFactory(
                        Config.MIN_PLAYERS,
                        Config.MAX_PLAYERS,
                        Config.MIN_PLAYERS
                )
        );
        sprNumberOfPlayers.valueProperty().addListener((obs, oldValue, newValue) -> this.setupValidatorAndVisibility());

        this.playerFields = List.of(
                this.tfPlayer1Name,
                this.tfPlayer2Name,
                this.tfPlayer3Name,
                this.tfPlayer4Name
        );
        this.playerFieldDefaultValueMap = Map.of(
                this.tfPlayer1Name, "Player 1",
                this.tfPlayer2Name, "Player 2",
                this.tfPlayer3Name, "Player 3",
                this.tfPlayer4Name, "Player 4"
        );
        this.playerFieldErrorLabelMap = Map.of(
                this.tfPlayer1Name, this.lblPlayer1Error,
                this.tfPlayer2Name, this.lblPlayer2Error,
                this.tfPlayer3Name, this.lblPlayer3Error,
                this.tfPlayer4Name, this.lblPlayer4Error
        );

        this.playerFieldLabelMap = Map.of(
                this.tfPlayer1Name, this.lblPlayer1,
                this.tfPlayer2Name, this.lblPlayer2,
                this.tfPlayer3Name, this.lblPlayer3,
                this.tfPlayer4Name, this.lblPlayer4
        );



        this.setupValidatorAndVisibility();
    }


    private void setupValidatorAndVisibility()
    {
        Map<TextField, Label> reducedFieldErrorLabelMap = new HashMap<>();
        AtomicInteger i = new AtomicInteger(0);
        this.playerFields.forEach((field) -> {
            Label errorLabel = this.playerFieldErrorLabelMap.get(field);
            Label fieldLabel = this.playerFieldLabelMap.get(field);
            if (i.getAndIncrement() < this.sprNumberOfPlayers.getValue()) {
                reducedFieldErrorLabelMap.put(field, errorLabel);
                field.setVisible(true);
                errorLabel.setVisible(true);
               fieldLabel.setVisible(true);
            } else {
                field.setText(this.playerFieldDefaultValueMap.get(field));
                field.setVisible(false);
                errorLabel.setText(null);
                errorLabel.setVisible(false);
                fieldLabel.setVisible(false);
            }
        });

        this.validator = new Validator(reducedFieldErrorLabelMap);

        this.validate();
    }

}
