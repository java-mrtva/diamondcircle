package util;

import javafx.scene.image.Image;
import models.game.cards.Card;
import models.game.cards.MoveCard;
import models.game.cards.SpecialCard;

import java.util.List;

public class Cards {
    public static final Image JOKER = new Image("resources/images/cards/joker.jpg");
    public static final Image KEMAL_1 = new Image("resources/images/cards/kemal(1).jpg");
    public static final Image JASAR_2 = new Image("resources/images/cards/jasar(2).jpg");
    public static final Image MITAR_3 = new Image("resources/images/cards/mitar(3).jpg");
    public static final Image MILE_4 = new Image("resources/images/cards/mile(4).jpg");

    public static final Card JOKER_CARD = new SpecialCard(JOKER);

    public static final List<Card> MOVE_CARDS = List.of(
            new MoveCard(1, KEMAL_1),
            new MoveCard(2, JASAR_2),
            new MoveCard(3, MITAR_3),
            new MoveCard(4, MILE_4)
    );
}
