package models.game.pieces;

import interfaces.abilities.Hoverable;
import javafx.application.Platform;
import javafx.scene.image.Image;
import models.MoveDescription;
import models.Position;
import models.Simulation;
import models.game.Piece;
import resources.Colors;
import ui_components.Cell;
import ui_components.ColorizedCell;
import ui_components.ImageCell;
import util.PieceColor;

import java.util.concurrent.CountDownLatch;

public class HoveringPiece extends Piece implements Hoverable {
    public HoveringPiece(PieceColor color) {
        super(color);
    }

    @Override
    public Cell getNewCell() {
        return new ImageCell(
                new Image(
                        String.format("resources/images/pieces/%s/hover_piece.png", this.color.getColor())
                )
        );
    }

    @Override
    protected void handleHole(Position position, MoveDescription moveDescription) {
        CountDownLatch latch2 = new CountDownLatch(1);
        position.setCell(Simulation.SIMULATION.getTrajectory().getNewCell());
        position.setElement(null);
        position.setLatchToWaitForJavaFx(latch2);
        position.setAsset(null);
        Platform.runLater(position);
        this.await(latch2);

        latch2 = new CountDownLatch(1);
        position.setCell(this.getNewCell());
        position.setElement(this);
        position.setLatchToWaitForJavaFx(latch2);
        position.setAsset(null);
        Platform.runLater(position);
        this.await(latch2);
    }
}
