package controllers;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import factories.SimulationFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import models.Position;
import models.Settings;
import models.Simulation;
import models.game.Piece;
import models.game.cards.Card;
import ui_components.PieceListCell;
import util.FileHelper;

public class SimulationController {

    public static SimulationController SIMULATION_CONTROLLER;
//    private Simulation simulation;
    public SimulationController(Settings settings) {
        this.settings = settings;
        Simulation.SIMULATION = SimulationFactory.generateSimulation(settings, this);
        SimulationController.SIMULATION_CONTROLLER = this;
    }

    private Settings settings;
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private Button btnPlayPause;

    @FXML
    private Button btnShowResultFiles;

    @FXML
    private GridPane gridPanePlayground;

    @FXML
    private GridPane gridPanePlayerNames;

    @FXML
    private ImageView imageViewCard;

    @FXML
    private Label lblCurrentTime;

    @FXML
    private Label lblNumberOfGamesPlayed;

    @FXML
    private Label lblCurrentDescription;
    @FXML
    private ListView<Piece> listViewPieces;

    @FXML
    private AnchorPane aPanePlayground;

    @FXML
    void playPauseToggle(ActionEvent actionEvent) {
        if(!Simulation.RUNNING) {
            Simulation.PAUSED = false;
//            Simulation.SIMULATION = SimulationFactory.generateSimulation(settings, this);
            Simulation.SIMULATION.startSimulation();
        } else {
            Simulation.PAUSED = !Simulation.PAUSED;
        }
    }

    @FXML
    void initialize() {
        assert btnPlayPause != null : "fx:id=\"btnPlayPause\" was not injected: check your FXML file 'simulation.fxml'.";
        assert btnShowResultFiles != null : "fx:id=\"btnShowResultFiles\" was not injected: check your FXML file 'simulation.fxml'.";
        assert gridPanePlayground != null : "fx:id=\"gridPanePlayground\" was not injected: check your FXML file 'simulation.fxml'.";
        assert gridPanePlayerNames != null : "fx:id=\"gridPlnePlayerNames\" was not injected: check your FXML file 'simulation.fxml'.";
        assert imageViewCard != null : "fx:id=\"imageViewCard\" was not injected: check your FXML file 'simulation.fxml'.";
        assert lblCurrentTime != null : "fx:id=\"lblCurrentTime\" was not injected: check your FXML file 'simulation.fxml'.";
        assert lblNumberOfGamesPlayed != null : "fx:id=\"lblNumberOfGamesPlayed\" was not injected: check your FXML file 'simulation.fxml'.";
        assert listViewPieces != null : "fx:id=\"listViewPieces\" was not injected: check your FXML file 'simulation.fxml'.";


        this.initGridPanePlayground();
        this.initGridPanePlayerNames();
        this.initList();
        Position.setGridPane(this.gridPanePlayground);
        this.setNumberOfGamesPlayed();
        Simulation.SIMULATION.Render();

    }

    private void initGridPanePlayerNames() {
        Simulation.SIMULATION.getPlayers().forEach(p -> {
            ColumnConstraints columnConstraints = new ColumnConstraints();
            columnConstraints.setHgrow(Priority.SOMETIMES);
            columnConstraints.setMinWidth(10);
            columnConstraints.setPrefWidth(100);

            this.gridPanePlayerNames.getColumnConstraints().add(columnConstraints);
            Label label = new Label();
            label.setText(p.getName());
            label.setTextFill(Color.valueOf(p.getPieceColor().getColor()));
            this.gridPanePlayerNames.add(label, Simulation.SIMULATION.getPlayers().indexOf(p), 0);
            GridPane.setHalignment(label, HPos.CENTER);
            GridPane.setValignment(label, VPos.CENTER);
        });
    }

    public void initList() {
        ObservableList<Piece> items = FXCollections.observableArrayList();
        this.listViewPieces.setItems(items);
        Simulation.SIMULATION.getPlayers().forEach(p -> {
            items.addAll(p.getPiecesQueue());
        });

        listViewPieces.setCellFactory(list -> new PieceListCell());
    }

    @FXML
    void pieceClicked(MouseEvent event) {
        Piece piece  = listViewPieces.getSelectionModel().getSelectedItem();

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/views/movement_history.fxml"));

            loader.setController(new MovementHistoryController(piece, this.settings));

            Scene scene = new Scene(loader.load());
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle(piece.getPlayer().getName() + "-" + "Piece: " + piece.getPieceNumber());
            stage.show();
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    private void initGridPanePlayground() {
        for (int i = 0; i < this.settings.getMatrixDimension(); i++) {
            ColumnConstraints columnConstraints = new ColumnConstraints();
            columnConstraints.setHgrow(Priority.SOMETIMES);
            columnConstraints.setMinWidth(10);
            columnConstraints.setPrefWidth(100);

            RowConstraints rowConstraints = new RowConstraints();
            rowConstraints.setMinHeight(10);
            rowConstraints.setPrefHeight(30);
            rowConstraints.setVgrow(Priority.SOMETIMES);
            this.gridPanePlayground.getColumnConstraints()
                    .add(columnConstraints);
            this.gridPanePlayground.getRowConstraints()
                    .add(rowConstraints);
        }
    }

    public void setImageViewCard(Card card) {
        this.imageViewCard.setImage(card.getImage());
    }

    public void setCardDescription(String description) {
        this.lblCurrentDescription.setText(description);
    }

    public void setLblCurrentTime(int duration) {
        this.lblCurrentTime.setText(duration + " s");
    }

    public void Render() {
        Simulation.SIMULATION.Render();
    }

    @FXML
    void btnShowResultFilesClicked(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../resources/views/game_history.fxml"));

            Scene scene = new Scene(loader.load());
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Game history");
            stage.show();
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public void setNumberOfGamesPlayed() {
        File[] files = new File(FileHelper.GAME_HISTORY_FOLDER).listFiles();
        this.lblNumberOfGamesPlayed.setText(
           "Number of games played: " + files.length
        );
    }
}
