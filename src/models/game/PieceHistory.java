package models.game;

import models.Position;
import models.Simulation;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class PieceHistory {
    private ArrayList<Position> positionsHistory;
    private Piece piece;
    private boolean hasFinished;

    public PieceHistory(Piece piece) {
        this.piece = piece;
        this.hasFinished = false;
        this.positionsHistory = new ArrayList<>();
    }

    public void stepOnPosition(Position position) {
        this.positionsHistory.add(position);
    }

    public void setFinished() {
        this.hasFinished = true;
    }

    public ArrayList<Position> getPositionsHistory() {
        return positionsHistory;
    }

    public String getPath() {
        return this.positionsHistory.stream().map(
                        position -> Integer.toString(position.getMatrixOrdinalNumber(
                                Simulation.SIMULATION.getSettings().getMatrixDimension()
                        ))
                )
                .collect(Collectors.joining("-"));
    }

    @Override
    public String toString() {
        return "Piece " + this.piece.getPieceNumber() +
                String.format(" (%s,%s) ", this.piece.getClass().getSimpleName(), this.piece.getPlayer().getPieceColor().getColor()) +
                " - " +
                String.format("path (%s)", this.getPath()) +
                " - " +
                String.format("finished (%s)", this.hasFinished ? "yes" : "no");
    }
}
