package models.game.cards;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;

public class DeckOfCards {
    private Queue<Card> cards;

    public DeckOfCards(LinkedList<Card> cards) {
        this.cards = cards;
    }

    public Card draw() {
        Card card = this.cards.poll();
        this.cards.add(card);
        return card;
    }
}
