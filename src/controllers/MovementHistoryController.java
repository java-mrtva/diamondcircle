package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import factories.TrajectoryGenerator;
import javafx.fxml.FXML;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import models.Settings;
import models.Simulation;
import models.Trajectory;
import models.game.Piece;
import resources.Colors;
import ui_components.ColorizedCell;

public class MovementHistoryController {

    private Piece piece;
    private Settings settings;

    private Trajectory trajectory;
    MovementHistoryController(Piece piece, Settings settings) {
        this.piece = piece;
        this.settings = settings;
    }
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private GridPane gridPanePlayground;

    @FXML
    void initialize() {
        assert gridPanePlayground != null : "fx:id=\"gridPanePlayground\" was not injected: check your FXML file 'movement_history.fxml'.";

        this.trajectory = new TrajectoryGenerator(this.settings.getMatrixDimension()).getTrajectory();
        this.initGridPanePlayground();
        this.renderTrajectory();
        this.renderPositions();
    }

    private void initGridPanePlayground() {
        for (int i = 0; i < this.settings.getMatrixDimension(); i++) {
            ColumnConstraints columnConstraints = new ColumnConstraints();
            columnConstraints.setHgrow(Priority.SOMETIMES);
            columnConstraints.setMinWidth(10);
            columnConstraints.setPrefWidth(100);

            RowConstraints rowConstraints = new RowConstraints();
            rowConstraints.setMinHeight(10);
            rowConstraints.setPrefHeight(30);
            rowConstraints.setVgrow(Priority.SOMETIMES);
            this.gridPanePlayground.getColumnConstraints()
                    .add(columnConstraints);
            this.gridPanePlayground.getRowConstraints()
                    .add(rowConstraints);
        }
    }

    private void renderTrajectory() {
        this.trajectory.getPositions().forEach(p -> {
            this.gridPanePlayground.add(new ColorizedCell(Colors.TRAJECTORY_COLOR), p.getY(), p.getX());
        });
    }

    private void renderPositions() {
        this.piece.getPieceHistory().getPositionsHistory().forEach(
                p -> {
                    this.gridPanePlayground.add(new ColorizedCell(this.piece.getPlayer().getPieceColor().getColor()), p.getY(), p.getX());
                }
        );
    }

}
