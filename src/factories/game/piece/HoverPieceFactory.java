package factories.game.piece;

import interfaces.functional.PieceGeneratable;
import models.game.Piece;
import models.game.pieces.HoveringPiece;
import util.PieceColor;

public class HoverPieceFactory implements PieceGeneratable {
    @Override
    public Piece generate(PieceColor color) {
        return new HoveringPiece(color);
    }
}
