Player 1 - Player 1
	Piece 1 (UberFastPiece,red)  - path (38-19-31-17-26) - finished (yes)
	Piece 2 (HoveringPiece,red)  - path (4-12-34-30-19-31-24) - finished (yes)
	Piece 3 (OrdinaryPiece,red)  - path (20-40) - finished (no)
	Piece 4 (UberFastPiece,red)  - path (12-16-33-25) - finished (yes)
Player 2 - Player 2
	Piece 1 (UberFastPiece,green)  - path (28-30-27-18) - finished (yes)
	Piece 2 (OrdinaryPiece,green)  - path (28-38) - finished (no)
	Piece 3 (UberFastPiece,green)  - path (20-46-16-27-39) - finished (no)
	Piece 4 (OrdinaryPiece,green)  - path (12-34-46-22-19-27-23-18) - finished (no)

Game duragion: 55 s