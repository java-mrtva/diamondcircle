package factories.game;

import models.game.Player;
import util.PieceColor;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class PlayersFactory {
    public static CopyOnWriteArrayList<Player> generatePlayers(List<String> playerNames) {
        CopyOnWriteArrayList<Player> players = new CopyOnWriteArrayList<>();

        for(int i = 0; i < playerNames.size(); i++) {
            players.add(new Player(playerNames.get(i), PieceColor.values()[i % PieceColor.values().length]));
        }

        return players;
    }
}
