package util;

import models.Settings;
import resources.Colors;

public enum PieceColor {
    RED, GREEN, BLUE, YELLOW;

    public String getColor() {
        return switch (this) {
            case RED -> Colors.PIECE_COLOR_RED;
            case GREEN -> Colors.PIECE_COLOR_GREEN;
            case BLUE -> Colors.PIECE_COLOR_BLUE;
            case YELLOW -> Colors.PIECE_COLOR_YELLOW;
        };
    }
}
