package models.game;

import factories.game.PiecesFactory;
import models.MoveDescription;
import models.game.cards.MoveCard;
import util.PieceColor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Player {
    private String name;
    private PieceColor pieceColor;
    private Queue<Piece> piecesQueue;
    private ArrayList<Piece> pieces;

    private int pieceNumber = 0;

    private Piece currentPiece;

    private boolean isFinished;

    public Player(String name, PieceColor pieceColor) {
        this.name = name;
        this.pieceColor = pieceColor;
        this.pieces = PiecesFactory.generatePieces(pieceColor);
        this.piecesQueue = new LinkedList<>();
        this.piecesQueue.addAll(this.pieces);

        AtomicInteger ordinary = new AtomicInteger(1);
        this.piecesQueue.forEach(p -> {
            p.setPlayer(this);
            p.setPieceNumber(ordinary.getAndIncrement());
        });

        this.isFinished = false;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public String getName() {
        return name;
    }

    public PieceColor getPieceColor() {
        return pieceColor;
    }

    public Queue<Piece> getPiecesQueue() {
        return piecesQueue;
    }

    public int getPieceNumber() {
        return pieceNumber;
    }

    public void move(MoveCard card, MoveDescription moveDescription) {
        if (this.currentPiece == null) {
            this.currentPiece = this.piecesQueue.poll();
            this.pieceNumber++;
            if (this.currentPiece == null) {
                return;
            }
//            this.currentPiece.enter(card);
        }
        moveDescription.setPiece(this.currentPiece);
        moveDescription.setPieceNumber(this.pieceNumber);
        this.currentPiece.move(card.getValue(), moveDescription);
        if (this.currentPiece.isFinished()) {
            this.currentPiece = this.piecesQueue.poll();
            this.pieceNumber++;
            if (this.currentPiece == null) {
                this.isFinished = true;
            }
        }
    }
    public String getLog(int ordinaryNumber) {
        return "Player " + ordinaryNumber + " - " + this.getName() + "\n" +
                this.pieces.stream().map(piece -> "\t" + piece.getPieceHistory().toString())
                        .collect(Collectors.joining("\n"));

    }
}
