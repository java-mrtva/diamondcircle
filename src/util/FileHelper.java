package util;

import models.Simulation;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileHelper {
    public static String GAME_HISTORY_FOLDER = "src/game_history";

    public static void writeGameHistory(Simulation simulation) throws IOException {
        File folder = new File(GAME_HISTORY_FOLDER);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy_HH-mm");
        var fileName = "GAME_" + simpleDateFormat.format(new Date()) + ".txt";
        File file = new File(GAME_HISTORY_FOLDER + java.io.File.separator + fileName);
        file.createNewFile();
        FileWriter fileWriter = new FileWriter(GAME_HISTORY_FOLDER + java.io.File.separator + fileName, true);

        fileWriter.append(simulation.toString());
        fileWriter.close();
    }

    public static String readGameHistory(String fileName) {
        StringBuilder content = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            String s;
            while ((s = in.readLine()) != null) {
                content.append(s + "\n");
            }
            in.close();

        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
        return content.toString();
    }
}
