package models;

import interfaces.Renderable;
import javafx.application.Platform;
import javafx.scene.image.Image;
import models.game.Piece;
import resources.Colors;
import ui_components.Cell;
import ui_components.ColorizedCell;
import ui_components.ImageCell;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Trajectory implements Renderable {
    protected CopyOnWriteArrayList<Position> positions;

    private final Queue<Piece> waitingPieces;

    public Trajectory(CopyOnWriteArrayList<Position> positions) {
        this.positions = positions;
        this.waitingPieces = new LinkedList<>();

    }

    public Cell getNewCell() {
        return new ColorizedCell(Colors.TRAJECTORY_COLOR);
    }

    public CopyOnWriteArrayList<Position> getPositions() {
        return positions;
    }

    public Position getNextPosition(Position position, int steps) { //TODO: move n times
        int nextIndex = ((position == null) ? -1 : this.positions.indexOf(position)) + steps;
//        int nextIndex = this.positions.indexOf(position) + steps;
        if (nextIndex >= this.positions.size()) {
            return null;
        }

        return this.positions.get(nextIndex);
    }

    @Override
    public void Render() {
        this.positions.forEach(Position::Render);
    }
}
