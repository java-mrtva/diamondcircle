package models.game.pieces;

import javafx.scene.image.Image;
import models.game.Piece;
import ui_components.Cell;
import ui_components.ImageCell;
import util.PieceColor;

public class OrdinaryPiece extends Piece {
    public OrdinaryPiece(PieceColor color) {
        super(color);
    }

    @Override
    public Cell getNewCell() {
        return new ImageCell(
                new Image(
                        String.format("resources/images/pieces/%s/ordinary_piece.png", this.color.getColor())
                )
        );
    }
}
