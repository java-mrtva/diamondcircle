import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Objects;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("resources/views/settings.fxml")));
        primaryStage.setTitle("Diamond Circle");
        primaryStage.setScene(new Scene(root));
        primaryStage.setOnCloseRequest(we -> {
//            Simulation.RUNNING = false;
            System.exit(0);
        });
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}