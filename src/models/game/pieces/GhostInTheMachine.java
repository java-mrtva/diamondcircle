package models.game.pieces;

import interfaces.abilities.Collectable;
import interfaces.functional.AssetGeneratable;
import interfaces.functional.PieceGeneratable;
import javafx.application.Platform;
import models.Position;
import models.Simulation;
import models.Trajectory;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GhostInTheMachine implements Runnable
{
    private Trajectory trajectory;
    private int matrixDimension;

    private AssetGeneratable generator;

    private CountDownLatch latch;

    public GhostInTheMachine(Trajectory trajectory, int matrixDimension) {
        this.trajectory = trajectory;
        this.matrixDimension = matrixDimension;
    }

    public int getRandomNumberOfAssets()
    {
        return ThreadLocalRandom.current().nextInt(2, this.matrixDimension);
    }
    public void placeAssets(AssetGeneratable generator)
    {
        this.generator = generator;
        (new Thread(this)).start();
//        this.latch.countDown();
    }

    public void removeAssets(Collectable collectable) {
        this.trajectory.getPositions().forEach(p -> {
            if (p.getAsset() != null && p.getAsset().getClass() == collectable.getClass() ) {
                p.setAsset(null);
                CountDownLatch latch = new CountDownLatch(1);

                p.setCell(Simulation.SIMULATION.getTrajectory().getNewCell());
                p.setLatchToWaitForJavaFx(latch);
                p.setAsset(null);
                Platform.runLater(p);
                try {
                    latch.await();
                } catch (InterruptedException exception) {
                    Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
                }
            }
        });
    }

    @Override
    public void run() {
        this.removeAssets(this.generator.generate());

        int count = this.getRandomNumberOfAssets();

        ArrayList<Position> positions = new ArrayList<>();

        while (positions.size() < count) {
            Position newPosition = this.trajectory.getPositions().get(new Random().nextInt(this.trajectory.getPositions().size()));
            while (newPosition.getAsset() != null || positions.contains(newPosition)) {
                newPosition = this.trajectory.getPositions().get(new Random().nextInt(this.trajectory.getPositions().size()));
            }
            newPosition.setAsset(generator.generate());

            positions.add(newPosition);
        }

        CountDownLatch latch = new CountDownLatch(positions.size());

        for (Position position : positions) {
            if(position.getAsset() != null) {
                position.setLatchToWaitForJavaFx(latch);
                position.setCell(position.getAsset().getNewCell());
                Platform.runLater(position);
            }
        }
        try {
            latch.await();
        } catch (InterruptedException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }


}
