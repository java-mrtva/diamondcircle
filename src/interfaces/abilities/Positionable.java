package interfaces.abilities;

import ui_components.Cell;

public interface Positionable {
    Cell getNewCell();
}
