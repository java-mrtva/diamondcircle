package config;

public class Config {
    public static int MIN_DIMENSION = 7;
    public static int MAX_DIMENSION = 10;
    public static int MIN_PLAYERS = 2;
    public static int MAX_PLAYERS = 4;

    public static int MAX_STRING_INPUT_LENGTH = 16;


}
