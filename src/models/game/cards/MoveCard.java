package models.game.cards;

import javafx.scene.image.Image;

public class MoveCard extends Card {
    private int value;

    public MoveCard(int value, Image image) {
        super(image);
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public void action() {

    }
}
