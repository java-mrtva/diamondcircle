package factories.game.assets;

import interfaces.abilities.Collectable;
import interfaces.functional.AssetGeneratable;
import interfaces.functional.PieceGeneratable;
import models.game.Piece;
import models.game.assets.Diamond;
import models.game.pieces.OrdinaryPiece;
import util.PieceColor;

public class DiamondFactory implements AssetGeneratable {
    @Override
    public Collectable generate() {
        return new Diamond();
    }
}