package factories.game;

import factories.game.piece.HoverPieceFactory;
import factories.game.piece.OrdinaryPieceFactory;
import factories.game.piece.UberFastPieceFactory;
import interfaces.functional.PieceGeneratable;
import models.game.Piece;
import util.PieceColor;

import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class PiecesFactory {

    private static Map<String, PieceGeneratable> pieceGeneratorsMap = Map.of(
            "ORDINARY", new OrdinaryPieceFactory(),
            "HOVER", new HoverPieceFactory(),
            "UBER_FAST", new UberFastPieceFactory()
    );
    public static ArrayList<Piece> generatePieces(PieceColor pieceColor)
    {
        return  generatePieces(pieceColor, 4);
    }

    public static ArrayList<Piece> generatePieces(PieceColor pieceColor, int count) {
        ArrayList<Piece> pieces = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            pieces.add(generateRandomPiece(pieceColor));
        }
        return pieces;
    }

    private static Piece generateRandomPiece(PieceColor pieceColor) {
        String pieceKey = (String) pieceGeneratorsMap.keySet().toArray()[(int) (new Random().nextInt(pieceGeneratorsMap.size()))];
        return pieceGeneratorsMap.get(pieceKey).generate(pieceColor);
    }
}
