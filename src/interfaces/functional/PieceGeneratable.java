package interfaces.functional;

import models.game.Piece;
import util.PieceColor;

public interface PieceGeneratable {
    Piece generate(PieceColor color);
}
