package models.game.assets;

import interfaces.abilities.Collectable;
import interfaces.abilities.Positionable;
import javafx.scene.image.Image;
import ui_components.Cell;
import ui_components.ImageCell;

public class Diamond implements Collectable {
    @Override
    public Cell getNewCell() {
        return new ImageCell(
                new Image(
                        "resources/images/assets/diamond.png"
                )
        );
    }
}
