package models;

import models.game.Piece;
import models.game.Player;

public class MoveDescription {
    private Player player;
    private Piece piece;

    private int pieceNumber;
    private Position startPosition;
    private Position nextPosition;

    private int numberOfFields;
    private boolean isFinished;



    private boolean hasFellIntoHoleOnStartPosition;
    private boolean isFellIntoHole;

    public MoveDescription(Player player) {
        this.player = player;
    }

    public MoveDescription setPlayer(Player player) {
        this.player = player;
        return this;
    }

    public MoveDescription setPiece(Piece piece) {
        this.piece = piece;
        return this;
    }

    public MoveDescription setPieceNumber(int pieceNumber) {
        this.pieceNumber = pieceNumber;
        return this;
    }

    public MoveDescription setNumberOfFields(int numberOfFields) {
        this.numberOfFields = numberOfFields;
        return this;
    }

    public MoveDescription setStartPosition(Position startPosition) {
        this.startPosition = startPosition;
        return this;
    }

    public MoveDescription setNextPosition(Position nextPosition) {
        this.nextPosition = nextPosition;
        return this;
    }

    public MoveDescription setFinished(boolean finished) {
        isFinished = finished;
        return this;
    }

    public MoveDescription setFellIntoHole(boolean fellIntoHole) {
        isFellIntoHole = fellIntoHole;
        return this;
    }

    public MoveDescription setHasFellIntoHoleOnStartPosition(boolean hasFellIntoHoleOnStartPosition) {
        this.hasFellIntoHoleOnStartPosition = hasFellIntoHoleOnStartPosition;
        return this;
    }

    public String generateDescription() {
        String description = "";

        description += this.player.getName() + " has a turn, with a piece "  +
                this.pieceNumber + String.format(" (%s)", piece.getClass().getSimpleName());


        if (this.startPosition == null) {
            description += ", enters the map";
        } else if (this.hasFellIntoHoleOnStartPosition) {
            description += ", stands on position " + this.startPosition.getMatrixOrdinalNumber(Simulation.SIMULATION.getSettings().getMatrixDimension())
                    + " instantly falls into the hole";
            return description;
        }
        else {
            description += ", moves out from position " + this.startPosition.getMatrixOrdinalNumber(Simulation.SIMULATION.getSettings().getMatrixDimension());
        }

        description += ", skipping " + this.numberOfFields + " field" + (this.numberOfFields > 1 ? "s" : "");

        if(this.piece.getNumberOfDiamonds() > 0) {
            description += String.format("(with %s diamonds)", this.piece.getNumberOfDiamonds());
        }

        if (this.nextPosition == null) {
            description += " and ends the circle.";
        } else {
            description += ", and gets into position " + this.nextPosition.getMatrixOrdinalNumber(Simulation.SIMULATION.getSettings().getMatrixDimension());
        }


        if (this.isFellIntoHole) {
            description += " and falls into the hole";
        }

        return description;
    }
}
