package models.game.cards;

import javafx.scene.image.Image;

public abstract class Card
{
    protected Image image;

    protected Card(Image image) {
        this.image = image;
    }

    public Image getImage() {
        return image;
    }

    public abstract void action();
}
