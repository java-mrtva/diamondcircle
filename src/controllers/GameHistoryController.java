package controllers;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import util.FileHelper;

public class GameHistoryController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ListView<String> listViewGames;

    @FXML
    private TextArea textAreaDetails;

    private File file = new File(FileHelper.GAME_HISTORY_FOLDER);
    private String content;

    @FXML
    void listClicked(MouseEvent event) {
        this.content = FileHelper.readGameHistory(FileHelper.GAME_HISTORY_FOLDER + "/" +
                listViewGames.getSelectionModel().getSelectedItem() + ".txt");
        this.textAreaDetails.setText(this.content);
    }

    public void initList() {
        ObservableList<String> items = FXCollections.observableArrayList();
        listViewGames.setItems(items);
        File[] files = this.file.listFiles();
        if (null != files && files.length > 0) {
            for (var el : files) {
                items.add(el.getName().replace(".txt", ""));
            }
        }
    }

    @FXML
    void initialize() {
        this.initList();
        textAreaDetails.setEditable(false);

        assert listViewGames != null : "fx:id=\"listViewGames\" was not injected: check your FXML file 'Untitled'.";
        assert textAreaDetails != null : "fx:id=\"textAreaDetails\" was not injected: check your FXML file 'Untitled'.";
    }

}
