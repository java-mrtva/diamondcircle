package util;

import java.util.Map;

public class Literals {
    public static String RULE_REQUIRED = "required";
    public static String RULE_UNIQUE = "unique";
    public static String RULE_MAX_LENGTH = "max_length";

    public static Map<String, String> ERROR_LABELS = Map.of(
            RULE_REQUIRED, "This field is required",
            RULE_UNIQUE, "This value already exists",
            RULE_MAX_LENGTH, "This value is too long"
    );
}
