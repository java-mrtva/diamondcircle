package models;

import controllers.SimulationController;
import factories.SimulationFactory;
import factories.game.assets.DiamondFactory;
import factories.game.assets.HoleFactory;
import interfaces.Renderable;
import javafx.application.Platform;
import models.game.Player;
import models.game.cards.Card;
import models.game.cards.DeckOfCards;
import models.game.cards.MoveCard;
import models.game.cards.SpecialCard;
import models.game.pieces.GhostInTheMachine;
import util.FileHelper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class Simulation implements Renderable, Runnable {

    public static Simulation SIMULATION;
    public static boolean RUNNING = false;
    public static boolean PAUSED = false;

    private int duration;
    private Settings settings;

    private CopyOnWriteArrayList<Player> players;

    private Queue<Player> playersQueue;

    private Trajectory trajectory;

    private DeckOfCards deckOfCards;

    private SimulationController controller;

    private GhostInTheMachine ghost;

    public static Handler LOG_HANDLER;
    public static Logger LOGGER = Logger.getLogger(Simulation.class.getName());

    {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
            Date date = new Date();
            Path fileName = Paths.get("src/logs" + java.io.File.separator + formatter.format(date) + "/simulation.log");
            if (!Files.exists(fileName.getParent())) {
                Files.createDirectory(fileName.getParent());
            }
            LOG_HANDLER = new FileHandler(fileName.toAbsolutePath().toString());
            LOGGER.addHandler(LOG_HANDLER);
        } catch (IOException exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }
    }

    public Simulation(Settings settings, CopyOnWriteArrayList<Player> players, Trajectory trajectory, DeckOfCards deckOfCards, SimulationController controller) {
        this.settings = settings;
        this.players = players;
        this.playersQueue = new LinkedList<>();
        this.playersQueue.addAll(this.players);
        this.trajectory = trajectory;
        this.deckOfCards = deckOfCards;
        this.controller = controller;
        this.ghost = new GhostInTheMachine(this.trajectory, this.settings.getMatrixDimension());
    }

    @Override
    public void Render() {
        this.trajectory.Render();
    }

    public CopyOnWriteArrayList<Player> getPlayers() {
        return players;
    }

    public void startSimulation() {
        Simulation.RUNNING = true;
        new Thread(this).start();
    }

    public Settings getSettings() {
        return settings;
    }

    public void run() {
        this.duration = 0;
        this.setDiamondPlacingTimer();
//        this.setDurationTimer();
        while (!this.playersQueue.isEmpty() && Simulation.RUNNING) {
            if(Simulation.PAUSED) {
                continue;
            }
           Player currentPlayer = this.playersQueue.poll();

           if(currentPlayer == null) {
               Simulation.RUNNING = false;
               break;
           }
            Card currentCard = this.deckOfCards.draw();
            String description = "";
            this.controller.setImageViewCard(currentCard);
           if(currentCard instanceof MoveCard) {
               MoveDescription moveDescription = new MoveDescription(currentPlayer);
               currentPlayer.move((MoveCard) currentCard, moveDescription);
               description = moveDescription.generateDescription();
           } else if (currentCard instanceof SpecialCard) {
               this.ghost.placeAssets(new HoleFactory());
               description = "Planting holes";
           }

           this.setMoveDescription(description);
            if (!currentPlayer.isFinished()) {
                this.playersQueue.add(currentPlayer);
            }

            try {
                this.duration++;
                updateDuration();
                Thread.sleep(1000);
            } catch (Exception exception) {
                Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
            }
        }

        Simulation.RUNNING = false;
        Simulation.PAUSED = true;
        try {
            FileHelper.writeGameHistory(this);
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }

        try {
            Platform.runLater(() -> {
                SimulationController.SIMULATION_CONTROLLER.setNumberOfGamesPlayed();
            });
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }

        Simulation.SIMULATION = SimulationFactory.generateSimulation(this.settings, SimulationController.SIMULATION_CONTROLLER);
        this.setMoveDescription("The simulation has finished.");

        try {
            Platform.runLater(() -> {
                SimulationController.SIMULATION_CONTROLLER.Render();
            });
        } catch (Exception exception) {
            Logger.getLogger(Simulation.class.getName()).log(Level.WARNING, exception.fillInStackTrace().toString());
        }

        System.out.println(this);
    }

    private void setDiamondPlacingTimer() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(!Simulation.PAUSED && Simulation.RUNNING) {
                    ghost.placeAssets(new DiamondFactory());
                }
            }
        }, 5000, 5000);
    }

    private void setDurationTimer() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    if (Simulation.RUNNING && !Simulation.PAUSED) {
                        duration++;
                        updateDuration();
                    }
                });
            }
        }, 1000, 1000);
    }


    public void setMoveDescription(String moveDescription) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                SimulationController.SIMULATION_CONTROLLER.setCardDescription(moveDescription);
            }
        });
    }

    public void updateDuration() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                SimulationController.SIMULATION_CONTROLLER.setLblCurrentTime(duration);
            }
        });
    }

    public Trajectory getTrajectory() {
        return trajectory;
    }

    @Override
    public String toString() {
        AtomicInteger i = new AtomicInteger(1);
        return this.players.stream().map(player -> player.getLog(i.getAndIncrement()))
                .collect(Collectors.joining("\n"))
                + "\n\n"
                + "Game duragion: " + this.duration + " s";
    }
}
