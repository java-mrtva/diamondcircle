package factories.game.cards;

import models.game.cards.Card;
import models.game.cards.DeckOfCards;

import java.util.Collections;
import java.util.LinkedList;

import static util.Cards.*;

public class DeckOfCardsGenerator {
    public static DeckOfCards getDeckOfCards() {
        LinkedList<Card> deck = new LinkedList<>();

        for (int i = 0; i < 10; i++) {
            deck.addAll(MOVE_CARDS);
        }

        for (int i = 0; i < 12; i++) {
            deck.add(JOKER_CARD);
        }

        Collections.shuffle(deck);
        return new DeckOfCards(deck);
    }
}
