package interfaces.functional;

import interfaces.abilities.Collectable;
import models.game.Piece;
import util.PieceColor;

public interface AssetGeneratable {
    Collectable generate();
}
